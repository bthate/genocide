manpage
=======

NAME
        GENOCIDE - the king of the netherlands commits genocide.

SYNOPSIS
        gcd <cmd> [mods=mod1,mod2] 

DESCRIPTION
        GENOCIDE is a python3 program that holds evidence that the king of the
        netherlands is doing a genocide, a written response where the king
        of the netherlands confirmed taking note of "what i have written", 
        proof that medicine like zyprexa, haldol, abilify and clozapine are
        poison.
        
        GENOCIDE shows correspondence with the Internationnal Criminal Court
        about the genocide of the king of th netherlands (using the law to
        adminiter poison), including stats on suicide while the king of
        the netherlands genocide is still going on.

        GENOCIDE provides a IRC bot that can run as a background daemon for 24/7
        a day presence in a IRC channel. You can use it to display RSS feeds,
        act as a UDP to IRC gateway, program your own commands for it and have
        it log objects on disk to search them.
        
        GENOCIDE is placed in the Public Domain, no COPYRIGHT, no LICENSE.

EXAMPLES
        1) gcd cfg server=<server> channel=<channel> nick=<nick>
        2) gcd met <userhost>
        3) gcd rss <url>
        4) gcd ftc
        5) gcd cmd
        6) gcd mods=irc

